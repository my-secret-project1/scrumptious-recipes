from django.db import models


# Create your models here.
class Recipe(models.Model):
    name = models.CharField(max_length=125)
    author = models.CharField(max_length=100)
    description = models.TextField()
    image = models.URLField(null=True, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Measure(models.Model):
    name = models.CharField(max_length=30, unique=True)
    abbreviation = models.CharField(max_length=10, unique=True)

    def __str__(self):
        return self.name


class FoodItem(models.Model):
    name = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.name

class Ingredient(models.Model):
    amount = models.FloatField(blank=False, null=True)
    recipe = models.ForeignKey(
        "Recipe",
        related_name="ingredients",
        on_delete=models.CASCADE,
        null=True,
    )
measure = models.ForeignKey("Measure", on_delete=models.PROTECT, null=True)
food = models.ForeignKey("FoodItem", on_delete=models.PROTECT, null=True)

def __str__(self):
    return self.amount


# Many to Many Example, dont put in yet
# class Keyword(models.Model):
#   name = models.CharField(max_length=30)
#   post = models.ManyToManyField("Post", related_name="keywords")
