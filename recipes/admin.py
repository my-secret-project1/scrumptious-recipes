from django.contrib import admin

from .models import Recipe
from .models import Measure
from .models import FoodItem

# Register your models here.

admin.site.register(Recipe)
admin.site.register(Measure)
admin.site.register(FoodItem)
